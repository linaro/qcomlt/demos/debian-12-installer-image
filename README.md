
# X1E80100 CRD - Debian Full Setup

## Initial setup
This guide relies on the Windows meta being already flashed on your CRD.

## Create Partitions
Boot to Windows. Once booted into Windows, open “Settings → Storage → Disk & Volumes” and resize the Windows (C:) partition by shrinking by at least 20GB. Then, create 2 partitions from unallocated space:

- boot - at least 500M (higher if you intend to have lots of kernels installed)
- root - at least 20G

## Setup the debian installer
Download the installer from here and flash it on some USB thumbdrive (at least 400M in size). It is based (but modified) on this netboot variant, so it needs internet connection. Then download Rufus tool (arm64) from https://rufus.ie/downloads/ and run it. Select the USB drive and then select the installer image file and click start.
After it is done, reboot the machine and boot from USB.

## Enable boot from USB
First you need to boot to EDL shell.

To do that, you’ll need to
- add a mapping in your SecureCRT (Options → Session Options → Terminal → Emulation → Mapped Keys”. Map “home” key to “\033\133\110”. 
- Then, after you reboot, you need to hit “home” key a couple of times in order to make the EDL menu show up.

Once in EDL menu, select ‘8 Boot USB First’, and then press ‘up’ key to enable. If at any point you want to disable it, follow the same procedure except press ‘down’ to disable.

Then reboot from the EDL menu, to make sure the changes are persistent.
## Install Debian
Make sure you have either an ethernet-to-usb adapter plugged in, with internet connection available. You can also set up wpa_supplicant if you want to use wifi (TBD).
Boot with USB installer media plugged in.
- At the grub menu, select “Install”.
- At the “Select your language” menu, pick your preference. Same at “Select your location”.
- At the “Configure the network” menu, select the ethernet adapter (if you have one) or wifi after you set up wpa_supplicant (TBD). If network is configured correctly, it will ask you next for a hostname name and then domain. Fill those or leave them as default, whichever your prefer.
- Then, at “Choose a mirror of the Debian archive”, pick your preference. Same for the mirror site. Next, leave the http proxy empty.

Then you’ll see a warning with the message “No kernel modules were found. …”. This is because we are using a custom kernel even for installer. So select “yes” to continue without loading the kernel modules.

Next, it asks you to set up a root password. And again to confirm it.

Then it asks you for a full name of the user and then the username. Next, it asks you for a password and a confirmation of that password.

Then you will be asked to select the timezone.

Next menu is the “Partition disks”. Select “Manual”. Then you need to format and select mount point for each of the boot and root partitions. They should be at the end of list. Look for the sizes. Select the ~500M, name it “boot”, then select “Use as:” and select ext4, select the mount point for it to be “/boot”, and select “Format the partition:” as “yes, format it”. Then select “Done setting up the partition”. Do the same for the root partition, but use the “root” name and the “/” as mount point instead. Then, select the “Finish partitioning and write changes to disk”. It’ll ask you about swap space, but we will continue without swap, so select “No”. Then you need to confirm the changes done to the partitions, so select “Yes” to write those changes to disk.

Next, the base system is installed. It shouldn’t take more than a couple of minutes. It will also install the debian archive provided image, but our hacked up installer will remove the installed files for that and install our custom kernel instead.

Next it will ask you if you want to participate in the package usage survey. Select “No”.

Next it will ask you select a desktop environment. Select both “Debian desktop environment” and “GNOME”. Depending on your network and usb adapter, it will take some time to retrieve ~1400 files. Then the installation goes on.

Then, at “Install the GRUB boot loader” warning, select “No” to not install EFI on the removable media path. Next, it will also ask you about updating the NVRAM variables automatically. Select “No” here as well.

Next, it is off to GRUB installation.

After that, the “Finish the installation” pop-up shows. Select “Continue”. You’ll see then a “Running install-custom-kernel” process. This is where the deb packaged kernel gets installed and the debian archive provided one is removed. This will also copy the dtb, will update the grub default cmdline and will add the devicetree line in the script that generates the grub.cfg.

Then, it reboot. Unplug the USB installation media.

It will take some time to boot as it will try USB first.

Since you do not have the proper EFI boot option configured yet, it will boot by default to Windows.

## Modify the EFI boot config option
 

Boot to EDL shell efi (or shell.efi app from installer media TBD) and modify/replace the first boot option in such a way that it points to efiesp partition, towards the EFI/Debian/grubaa64.efi.

To do that, once you are in EFI Shell, select the ESP partition as below:
```
Shell> map -r -b
Shell> fs11:
FS11:\> ls EFI\debian
```
This should list grubaa64.efi file. If this doesn’t show the files above, try other values for fsXX: (XX for values 1…) in step above and repeat. Note you should keep searching fsXX: until you find this directory structure even as high as FS15: or more

Modify EFI variable Boot#### to get it to point to DtbLoader.efi:
```
FS11:\> bcfg boot add  0 EFI\Debian\grubaa64.efi “Debian[f]”
FS11:\> bcfg boot dump
 Option: 00, Variable: Boot0002
     Desc     - Debian
     Devpath - <Path to efiesp partition>\EFI\debian\grubaa64.efi
     ...
```
Exit from Shell to EDL menu
```
FS11:\> exit
```
Select Reboot from EDL Menu, to make sure the changes are persistent.

Now it should boot to grub first and the default (first option there) should be Debian.
Just to be sure, press ‘e’ to edit the Debian menu entry and check for the 'devicetree /x1e80100-crd.dts’ and that the kernel arguments contain “pd_ignore_unused clk_ignore_unused fw_devlink=off”. Press Fn+F10 to boot.

## Switch to SID
After a fresh install, edit the /etc/apt/sources.list to look like this:
```
deb http://deb.debian.org/debian/ sid main
deb-src http://deb.debian.org/debian/ sid main
#deb http://security.debian.org/debian-security trixie-security main
#deb-src http://security.debian.org/debian-security trixie-security main

# trixie-updates, to get updates before a point release is made;
# see https://www.debian.org/doc/manuals/debian-reference/ch02.en.html#_updates_and_backports
#deb http://deb.debian.org/debian/ trixie-updates main
#deb-src http://deb.debian.org/debian/ trixie-updates main

deb [trusted=yes] https://download.opensuse.org/repositories/home:lumag_lumag/Debian_Unstable/ ./
```
Basically, we comment out the trixie-updates and trixie-security lines, then change trixie with sid in the first two lines. There are no sid-security and sid-updates repositories, therefore comment out. Also, add at the end the Dmitry’s debian repository from opensuse mirror that contains the mesa libs modified to work with X1E80100’s GPU.

Then you need to do:
```
$ sudo apt update
$ sudo apt dist-upgrade 
```
During the dist-ugrade, it will stop asking you what to do about your old /etc/grub.d/10_linux file, as there is a new version coming with the upgrade. Select 'N' and after the dist-upgrade fails, apply the following change to the /etc/grub.d/10_linux.dpkg-dist:
```
--- /etc/grub.d/10_linux        2023-10-09 19:35:36.015770731 +0000
+++ /etc/grub.d/10_linux.dpkg-dist      2023-10-09 19:35:59.786076896 +0000
@@ -177,6 +177,15 @@
     fi
     printf '%s\n' "${prepare_boot_cache}" | sed "s/^/$submenu_indentation/"
   fi
+
+  #### HACK to add devicetree line ####
+  message="$(gettext_printf "devicetree /x1e80100-crd.dtb" ${version})"
+  sed "s/^/$submenu_indentation/" << EOF
+       $message
+EOF
+  #####################################
+
+
   if [ x"$quiet_boot" = x0 ] || [ x"$type" != xsimple ]; then
     message="$(gettext_printf "Loading Linux %s ..." ${version})"
     sed "s/^/$submenu_indentation/" << EOF
```
Then, rename /etc/grub.d/10_linux.dpkg-dist to /etc/grub.d/10_linux.

Then re-run:
```
$ sudo apt dist-upgrade
Then, modify the /etc/environment to look like the following:
```

```
#MESA_LOADER_DRIVER_OVERRIDE=kms_swrast
MESA_LOADER_DRIVER_OVERRIDE=zink
TU_DEBUG=sysmem,noubwc,nolrz,nobin,flushall,syncdraw
```
## Modifying installer (optional)
Skip this entire section if you only intend to install Debian on your X1E80100 CRD, without doing any changes to the installer.
Keep in mind that this is a hacked up installer.  It is also modified so it can be writable so that you can always change it (the partition is writable) without needing to rebuild the whole thing.

### Repack installer initrd
The modules are part of the initrd. So if you intend to bring your own kernel to the installer you will have to repack it.
Create a folder for the unpacked initrd, copy the initrd.gz (not in the folder for the unpacked initrd) from the installer drive and unpack it running. Replace paths accordingly in the snippeds bellow
```
$ cd <folder for unpacked initrd>
$ gzip -d <path to old>/initrd.gz
$ cpio -id < <path to old>/initrd
```

For repacking do the following:
```
$ find . | cpio --create --format='newc' > <path to new>/inird
$ gzip -f <path to new>/inird
```
After this, you should have the newly repacked initrd.gz. Replace the old initrd.gz from your installer media with this newly repacked one.

### Replace installer kernel
This refers to replacing the kernel your Debian installer will boot with. For this, you need to copy your Image.gz to the installer install.a64 folder on your USB drive, with the name linux. Then copy your dtb in the dtb folder, overriding the old dtb. For the modules, you need to copy them into initrd, in the lib/modules/, and then repack the initrd again.

### Replace the installed kernel
If the installer kernel is good enough for you (meaning you’re able to reach installer menu, not just grub menu), then you’ll probably only want to replace the kernel that your Debian will be installed with. The way that this installer installs the custom kernel is via a finish-install.d hook. This hook will install the kernel image and modules that are provided as a debian package, found in the initrd and will remove the already installed kernel (which comes from debian archive). So in order to repack the initrd with your own kernel deb package, you’ll need to build your kernel with the following command:
```
$ make -j 16 bindeb-pkg
```
Once done you will end up with a couple of debian packages.
The kernel is ../linux-image-*_arm64.deb.

Copy that into the root folder of the unpacked initrd. Then you need modify the hook to point to this instead of the old deb package (which you can remove). The hook is found in:

<root of unpacked initrd>/usr/lib/finish-install.d/60install-custom-kernel
Change the variable $FILE_PATH_CUSTOM_KERNEL_DEB to point to your deb instead.
Copy the dtb as well. Also in the root folder of the of the unpacked initrd.
Then repack the initrd and copy it into the installer.

### Add or replace firmware
If you intend to add/replace any firmware, you need to copy yours into the lib/firmware/ folder in the unpacked initrd. Then repack and replace the initrd.gz on the installer drive with your newly packed one.

## Compiling your own grub
The grub repo can be found [here](https://savannah.gnu.org/git/?group=grub). Clone it and follow below steps to build it.

One important thing to keep in mind with respect to grub is that, depending on the boot partition, it needs to have a different $prefix variable (part of the binary). The $prefix variable points to the grub folder which should contain a valid grub.cfg and (optional) a grub.d folder. So in order to change prefix you either recompile the grub like described below and pass the correct prefix to the grub-mkimage, or you do grub-install from a running Debian.
```
$ ./bootstrap
$ ./configure --target=aarch64-linux-gnu
$ make -j16
$ ./grub-mkimage --directory grub-core --prefix '(hd0,msdos1)/boot/grub'   --output BOOTAA64.EFI --format arm64-efi part_gpt part_msdos ntfs ntfscomp hfsplus fat ext2 normal chain boot configfile linux minicmd gfxterm all_video efi_gop video_fb font video loadenv disk test gzio bufio gettext terminal crypto extcmd boot fshelp search iso9660
```
After this, your grub binary will be the ./BOOTAA64.EFI . Copy it on the EFI partition or the installer media.
